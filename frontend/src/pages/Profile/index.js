import React, { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { FiPower, FiTrash2 } from "react-icons/fi";

import logoImg from "../../assets/logo.png";

import api from "../../services/api";

import "./styles.css";

export default function Profile() {
	const [incidents, setIncidents] = useState([]);

	const history = useHistory();
	const ongId = localStorage.getItem("ongId");
	const ongName = localStorage.getItem("ongName");

	//pega dados da api (banco de dados) para mostrar na pagina web

	useEffect(() => {
		api
			.get("profiles", {
				headers: {
					Authorization: ongId,
				},
			})
			.then((response) => setIncidents(response.data));
	}, [ongId]);

	//função para deletar dados da api (banco de dados)

	async function handleDeleteIncident(id) {
		try {
			await api.delete(`incidents/${id}`, {
				headers: {
					Authorization: ongId,
				},
			});

			setIncidents(incidents.filter((incident) => incident.id !== id));
		} catch (error) {
			alert(`Erro ao deletar, tente novamente.`);
		}
	}

	function handleLogout() {
		localStorage.clear();

		history.push("/");
	}

	return (
		<div className="profile-container">
			<header>
				<img src={logoImg} />
				<span>Bem-vindo(a), {ongName}</span>

				<Link className="button" to="/incidents/new">
					Cadastrar Dúvida
				</Link>
				<button className="lixeira" onClick={handleLogout} type="button">
					<FiPower size={18} />
				</button>
			</header>

			<h1>Casos Cadastrados</h1>

			<ul>
				{incidents.map((incident) => (
					<li key={incident.id}>
						<strong>CASO:</strong>
						<p>{incident.title}</p>

						<strong>DESCRIÇÃO:</strong>
						<p>{incident.description}</p>

						<strong>VALOR:</strong>
						<p>
							{Intl.NumberFormat("pt-BR", {
								style: "currency",
								currency: "BRL",
							}).format(incident.value)}
						</p>

						<button
							className="lixeira"
							onClick={() => handleDeleteIncident(incident.id)}
							type="button"
						>
							<FiTrash2 size={20} color="#a8a8b3" />
						</button>
					</li>
				))}
			</ul>
		</div>
	);
}
