import React, { useEffect, useState } from "react";
import {
	View,
	Image,
	Text,
	TouchableOpacity,
	FlatList,
	StatusBar,
} from "react-native";
import { Feather } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

import api from "../../services/api";

import logoImg from "../../assets/logo.png";

import styles from "./styles";

export default function Incidents() {
	const [incidents, setIncidents] = useState([]);
	const [total, setTotal] = useState(0);

	const [page, setPage] = useState(1);
	const [loading, setLoading] = useState(false);

	const navigation = useNavigation();

	function navigateToDetail(incident) {
		navigation.navigate("Detail", { incident });
	}

	//carrega casos para mostrar no feed de perguntas
	async function loadIncidents() {
		if (loading) return;

		if (total > 0 && incidents.length === total) return;

		setLoading(true);

		const response = await api.get("incidents", {
			params: { page },
		});

		setIncidents([...incidents, ...response.data]);
		setTotal(response.headers["x-total-count"]);
		setPage(page + 1);
		setLoading(false);
	}

	useEffect(() => {
		loadIncidents();
	}, []);

	return (
		<View style={styles.container}>
			<StatusBar backgroundColor="#f0f0f5" barStyle="dark-content" />
			<View style={styles.header}>
				<Image source={logoImg} style={styles.logoImg} />
				<Text style={styles.headerText}>
					Total de <Text style={styles.headerTextBold}>{total} dúvidas</Text>.
				</Text>
			</View>

			<Text style={styles.title}>Bem-vindo!</Text>
			<Text style={styles.description}>
				Escolha um dos casos abaixo e ajude um aluno Feiano!
			</Text>

			<FlatList
				data={incidents}
				style={styles.incidentList}
				keyExtractor={(incident) => String(incident.id)}
				showsVerticalScrollIndicator={false}
				onEndReached={loadIncidents}
				onEndReachedThreshold={0.2}
				renderItem={({ item: incident }) => (
					<View style={styles.incident}>
						<Text style={styles.incidentProperty}>NOME:</Text>
						<Text style={styles.incidentValue}>{incident.name}</Text>

						<Text style={styles.incidentProperty}>DÍVIDA:</Text>
						<Text style={styles.incidentValue}>{incident.title}</Text>

						<Text style={styles.incidentProperty}>VALOR:</Text>
						<Text style={styles.incidentValue}>
							{Intl.NumberFormat("pt-BR", {
								style: "currency",
								currency: "BRL",
							}).format(incident.value)}
						</Text>

						<TouchableOpacity
							style={styles.detailsButton}
							onPress={() => navigateToDetail(incident)}
						>
							<Text style={styles.detailsButtonText}>Ver mais detalhes</Text>
							<Feather name="arrow-right" size={16} color="#006eab" />
						</TouchableOpacity>
					</View>
				)}
			/>
		</View>
	);
}
