![enter image description here](./frontend/src/assets/logo.png)




Fei Overflow é um projeto da matéria de Engenharia de Software do curso de Ciências da Computação do Centro Universitário FEI


## Prerequisitos


Utilize algum instalador de pacotes como yarn ou npm para escrever os comandos a seguir no cmd


## Instalação



### Backend 

Abra a pasta `./backend/`  no cmd e digite: 

```
npm install
```
```
npm start

```

### Frontend 


Abra a pasta `./frontend/` no cmd e digite: 


```
yarn install
```
```
yarn start
```
### Mobile 

Abra a pasta `./mobile/` no cmd e digite: 


```
yarn install
```
```
yarn start
```

#### Feito a partir de:
- Node.js
- React
- React Native
- SQLite

![enter image description here](./frontend/src/assets/heroes.png)
